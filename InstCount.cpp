//===-- InstCount.cpp - Collects the count of all instructions ------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This pass collects the count of all instructions and reports them
//
//===----------------------------------------------------------------------===//

#include "llvm/Analysis/Passes.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/InstVisitor.h"
#include "llvm/Pass.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/Dominators.h"
#include "llvm/Analysis/LoopInfo.h"

using namespace llvm;

#define DEBUG_TYPE "instcount"

STATISTIC(TotalInsts , "Number of instructions (of all types)");
STATISTIC(TotalBlocks, "Number of basic blocks");
STATISTIC(TotalFuncs , "Number of non-external functions");
STATISTIC(TotalMemInst, "Number of memory instructions");

#define HANDLE_INST(N, OPCODE, CLASS) \
  STATISTIC(Num ## OPCODE ## Inst, "Number of " #OPCODE " insts");

#include "llvm/IR/Instruction.def"


class ForList {
    DebugLoc start;
    DebugLoc end;
    int memCount;
    int compCount;

public:
    ForList() { memCount = 0; compCount = 0; };
    ForList(DebugLoc s, DebugLoc e) { start = s; end = e; memCount = 0; compCount = 0; }
    DebugLoc getStart() { return start; }
    DebugLoc getEnd() { return end; }
    void setStart(DebugLoc l) { start = l; }
    void setEnd(DebugLoc e) { end = e; }
    void incMemCount() { memCount++; }
    void incCompCount() { compCount++; }
    int getMemCount() { return memCount; }
    int getCompCount() { return compCount; }
    bool is_within(DebugLoc d) {
        if(d){
            if(start.getLine() < d.getLine() && d.getLine() < end.getLine())
                return true;
            if(start.getLine() == d.getLine() && start.getCol() <= d.getCol())
                return true;
            if(d.getLine() == end.getLine() && d.getCol() <= end.getCol())
                return true;
        }
        return false;
    }
};

std::vector<ForList> for_list;

namespace {
  class InstCount : public FunctionPass, public InstVisitor<InstCount> {
    friend class InstVisitor<InstCount>;

    void visitFunction(Function &F) { 
        ++TotalFuncs; 
        DominatorTree* DT = new DominatorTree(F);         
        LoopInfoBase<BasicBlock, Loop>* KLoop = new LoopInfoBase<BasicBlock, Loop>(); 
        KLoop->releaseMemory(); 
        KLoop->analyze(*DT);

        int cnt = 0;
        errs() << "Function - " << F.getName() << "\n";
        for(LoopInfoBase<BasicBlock, Loop>::reverse_iterator i = KLoop->rbegin(); 
                i != KLoop->rend(); i++) {
            errs() << "\n";
            Loop::LocRange l = (*i)->getLocRange();
            ForList *fl = new ForList(l.getStart(), l.getEnd());
            for_list.push_back(*fl);
            /*if(l.getStart()) {
                errs() << "Start - " << l.getStart().getLine() << "," << l.getStart().getCol();
                errs() << "\nEnd - ";
                errs() << l.getEnd().getLine() << "," << l.getEnd().getCol();
                errs() << "\n" << ++cnt << " " << "\n";
            }
            (*i)->dump();
            errs() << "\n";*/
            for(BasicBlock *b : (*i)->getBlocks()) {
                errs() << "*";
                if(b->hasName())
                    errs() << b->getName();
                else
                    errs() << "No Name";

                errs() << " - " << b->size() << "\n";
            }
            errs() << "\n";
        }
        //KLoop->print(errs());
        errs() << "\n";
        errs() << "\n";
    }
    void visitBasicBlock(BasicBlock &BB) { ++TotalBlocks; }

    bool isMemInst(Instruction *I) {
        if(dyn_cast<GetElementPtrInst>(I) || dyn_cast<LoadInst>(I) || 
                dyn_cast<StoreInst>(I) || dyn_cast<CallInst>(I) || 
                dyn_cast<InvokeInst>(I) || dyn_cast<AllocaInst>(I))
            return true;
        return false;
    }

    void add_to_list(Instruction *I) {
        for(std::vector<ForList>::iterator it = for_list.begin();
                it != for_list.end(); it++) {
            if(it->is_within(I->getDebugLoc())) {
                if(isMemInst(I)) it->incMemCount();
                else it->incCompCount();
                break;
            }
        }
    }

#define HANDLE_INST(N, OPCODE, CLASS) \
    void visit##OPCODE(CLASS &I) { \
        ++Num##OPCODE##Inst; \
        ++TotalInsts; \
        add_to_list(&I); \
       /* errs() << I.getOpcodeName() << " - "; \
        if(I.getDebugLoc()){\
        errs() << I.getDebugLoc().getLine() << ";"; \
        errs()<< I.getDebugLoc().getCol(); \
        } errs() << "\n"; */\
    }

#include "llvm/IR/Instruction.def"
    void visitInstruction(Instruction &I) {
      errs() << "Instruction Count does not know about " << I;
      llvm_unreachable(nullptr);
    }
  public:
    static char ID; // Pass identification, replacement for typeid
    InstCount() : FunctionPass(ID) {
      initializeInstCountPass(*PassRegistry::getPassRegistry());
    }

    bool runOnFunction(Function &F) override;

    void getAnalysisUsage(AnalysisUsage &AU) const override {
      AU.setPreservesAll();
    }
    void print(raw_ostream &O, const Module *M) const override { }
  };
}

char InstCount::ID = 0;
//INITIALIZE_PASS(InstCount, "instcount",
//                "Counts the various types of Instructions", false, true)

FunctionPass *llvm::createInstCountPass() { return new InstCount(); }

// InstCount::run - This is the main Analysis entry point for a
// function.
//
bool InstCount::runOnFunction(Function &F) {
    unsigned StartMemInsts = 
        NumGetElementPtrInst + NumLoadInst + NumStoreInst + NumCallInst +
        NumInvokeInst + NumAllocaInst;
    printf("StartMemInsts - %d\n", StartMemInsts);
  visit(F);
    unsigned EndMemInsts =
        NumGetElementPtrInst + NumLoadInst + NumStoreInst + NumCallInst +
        NumInvokeInst + NumAllocaInst;
    printf("EndMemInsts - %d\n", EndMemInsts);
  TotalMemInst += EndMemInsts-StartMemInsts;
  for(std::vector<ForList>::iterator it = for_list.begin();
          it != for_list.end(); it++) {
      if(it->getStart()) {
        errs() << it->getStart().getLine() << ":" << it->getStart().getCol();
        errs() << "-" << it->getEnd().getLine() << ":" << it->getEnd().getCol();
      }
      errs() << " " << it->getMemCount() << "+" << it->getCompCount() << "\n";
  }
  return false;
}

static RegisterPass<InstCount> X("instcount1", "Instruction Count Pass");
